
import math
import sys
import argparse

from typing import Dict, Tuple, List
from numpy.core.fromnumeric import argmax, shape
from numpy.lib.function_base import angle
from simulation import unity_simulator
import time
import random
import pprint
import numpy as np
comm = unity_simulator.UnityCommunication()

def findNodes(**kwargs):
	s, graph = comm.environment_graph()
	if len(kwargs) == 0:
		return None
	else:
		k, v = next(iter(kwargs.items()))
		return [n for n in graph['nodes'] if n[k] == v]

def findFirstNode(**kwargs):
	s, graph = comm.environment_graph()
	if len(kwargs) == 0:
		return None
	else:
		k, v = next(iter(kwargs.items()))
		for n in graph['nodes']:
			if n[k] == v:
				return n
		assert False, "No node found for "+str(k)+"|"+str(v)

def createMoveAgentsScript(liste: Dict[int, int]) -> List[str]:
	global basicScript

	s, graph = comm.environment_graph()
	script = basicScript
	for agentId, roomId in liste.items():
		roomName=roomDict[roomId]

		script.append("<char{}> [walktowards] <{}> ({}) :{}:".format(agentId-1, roomName, roomId, 0.5))
	basicScript = []
	return script

#returns all agent-ids in a room
def agentsInRoom(roomID: int) -> List[int]:
	global agentCount
	agents = []
	s, graph = comm.environment_graph()
	for edge in graph['edges']:
		from_id = edge['from_id']
		relation_type = edge['relation_type']
		to_id = edge['to_id']
		if(from_id <= agentCount):
			if(relation_type=='INSIDE' and to_id==roomID):
				agents.append(from_id)
	return agents

def spawnAgents(count: int, inOneRoom: bool) -> None:
	global agentInitiative, roomDict
	char_list = ['Chars/Male1','Chars/Male2','Chars/Male3','Chars/Male4','Chars/Female1','Chars/Female2','Chars/Female3','Chars/Female4']
	for id in range(count):
		if inOneRoom:
			room=list(roomDict.values())[0]
		else:
			room=random.choice(list(roomDict.values()))
		
		if id<len(char_list):
			comm.add_character(char_list[id], initial_room=room)
		else:
			comm.add_character(random.choice(char_list), initial_room=room)

def agentsInSight(agent: int) -> List[int]:
	global agentCount
	agents = []
	s, graph = comm.environment_graph()
	for edge in graph['edges']:
		from_id = edge['from_id']
		relation_type = edge['relation_type']
		to_id = edge['to_id']
		if(from_id == agent):
			if(to_id < agentCount and (relation_type=='FACING' or relation_type=='CLOSE')):
				agents.append(to_id)
	return agents

def observeAgent(observer: int, observed: int) -> None:
	global initiativeBelief, activeAgents
	factor = 2 #1: no change, >1: change, <1: inverted behaviour
	if(observed not in activeAgents.keys()):
		#printOutput("Agent", observer, "observed Agent", observed, "waiting")
		initiativeBelief[observer-1][observed-1] *= (1/5*factor)
	else:
		#prinOutputt("Agent", observer, "observed Agent", observed, "moving")
		initiativeBelief[observer-1][observed-1] = math.tanh(factor*initiativeBelief[observer-1][observed-1])

def chooseNextRoom(oldRoomId: int, agent:int) -> int:
	global activeAgents, roomDict, roomBelief, randomRoom
	if agent in activeAgents.keys():
		# when the agent has a room to walk to, it should not be changed
		return activeAgents[agent]

	if targetRoom == "A":
		nextRoomId = oldRoomId
		while(nextRoomId == oldRoomId):
			nextRoomId = random.choice(list(roomDict.keys()))
	else:
		rooms = sorted(zip(roomBelief[agent-1],sorted(roomDict.keys())))
		for room in rooms:
			if(room[1] != oldRoomId):
				nextRoomId = room[1]
				break
	return nextRoomId

#returns the id of the object to walk to (to prevent blocking the doors)
#generates unique solutions based on agentId
def chooseObjectInRoom(roomId: int, agentId: int)  -> Tuple[int, str]:
	s, graph = comm.environment_graph()
	firstSolution = None
	count = 1
	for edge in graph['edges']:
		from_id = edge['from_id']
		relation_type = edge['relation_type']
		to_id = edge['to_id']
		if relation_type == "INSIDE" and to_id == roomId:
			node = findFirstNode(id=from_id)
			if node["category"] == 'Furniture':
				if count == agentId:
					return from_id, node["class_name"]
				if count == 1:
					firstSolution = (from_id, node["class_name"])
				count += 1

	assert firstSolution is not None, "No object found in"+str(firstSolution)
	return firstSolution

"""
This part calculates for all agents whether the initiative of the current agent is larger than the initiative of all other agents.
If it is, the current agent is supposed to walk.
"""
def processAgent(agent: int) -> int:
	global initiativeBelief, activeAgents, roomDict, roomBelief, targetRoom, pathStrategy
	currentRoom = getCurrentRoom(agent)
	nextRoom = None
	agentsInCurrentRoom = agentsInRoom(currentRoom)
	printOutput("\n\nprocessing Agent 00{}".format(agent))
	printOutput("Agent in room ", roomDict[currentRoom], " with index ", currentRoom)
	printOutput("Agents in sight ", agentsInCurrentRoom)
	for index in range(len(initiativeBelief[agent-1])):
		printOutput("My prior belief of agent ", index + 1, " is ", initiativeBelief[agent-1][index])

	# update beliefs about the initiative of other agents
	for a in agentsInCurrentRoom:
		if(agent != a):
			observeAgent(agent, a)
	
	# update beliefs about the number of agents in the rooms
	if(targetRoom == "B"):
		roomBelief[agent-1][sorted(roomDict.keys()).index(currentRoom)] *= len(agentsInCurrentRoom)/(agentCount//getRoomCount())
	elif(targetRoom == "C"):
		roomBelief[agent-1][sorted(roomDict.keys()).index(currentRoom)] = len(agentsInCurrentRoom)

	for index in range(len(initiativeBelief[agent-1])):
		printOutput("My posterior belief of agent ", index + 1, " is ", initiativeBelief[agent-1][index])

	if(agent in activeAgents.keys() and activeAgents[agent] == currentRoom):
		# agent has reached its goal
		activeAgents.pop(agent)

		# evasive maneuver: when entering the target room, the agent should not block the door. 
		# Instead they choose an object in the room and move to this object
		targetId, targetName = chooseObjectInRoom(currentRoom, agent)

		# needs to be changed here, because the pipline does not support walking towards objects (?)
		basicScript.append("<char{}> [walk] <{}> ({})".format(agent-1, targetName, targetId))
		printOutput("Agent reached their goal and will walk to ", targetName)
		return None
			
	# agents have knowledge, how many of them should be in a room
	if len(agentsInCurrentRoom) > agentCount // getRoomCount(): 
		printOutput("Too many people in ", roomDict[currentRoom], ": ", agentsInCurrentRoom," | ", agentCount//getRoomCount())
		# the agent themself has a higher initiative that the other agents in the room
		if all([(initiativeBelief[agent-1, agent-1] >= initiativeBelief[agent-1, a-1]) for a in agentsInCurrentRoom]):
			#choose next target room
			nextRoom = chooseNextRoom(currentRoom, agent)
			printOutput("I will move to " + roomDict[nextRoom] + "!")
			printOutput("In Function ProcessAgent, we're the chosen one (", agent, "), next room is ", nextRoom)
			activeAgents[agent]= nextRoom
			printOutput("activeAgents: ", agent, activeAgents[agent])
			return nextRoom
		else:
			printOutput("but I won't move! ", initiativeBelief[agent-1, agent-1], [initiativeBelief[agent-1, a-1] for a in agentsInCurrentRoom] )
			# path to pop and return None
			pass
	else:
		if(pathStrategy == "stubborn" and agent in activeAgents):
			return activeAgents[agent]
		else: # abort strategy or no target
			printOutput("I will stay in this room.")
			# path to pop and return None
			pass
				
	# agent does not move this turn
	# remove inactive agent from activeAgents
	if(agent in activeAgents.keys()):
		activeAgents.pop(agent)
		printOutput("activeAgents: ", agent, " was removed")
	return None

def getRoomCount() -> int:
	global roomDict
	return len(roomDict.keys())

def getCurrentRoom(agent: int) -> int:
	s, graph = comm.environment_graph()
	for edge in graph['edges']:
		from_id = edge['from_id']
		relation_type = edge['relation_type']
		to_id = edge['to_id']
		if(from_id == agent and relation_type =="INSIDE"):
			return to_id
	assert False, "current room for agent "+str(agent)+" not found"

def calculateStep() -> Dict[int, int]:
	global agentCount
	moves = {}
	for id in range(1, agentCount+1):
		nextRoom = processAgent(id)
		if nextRoom is not None:
			moves[id] = nextRoom
	return moves # {(0, "bathroom"),(1,"kitchen"), (2, "kitchen")}

def initRoomDict() -> Dict[int, str]:
	roomDict = {}
	rooms = findNodes(category="Rooms")
	for room in rooms:
		roomDict[room['id']] = room['class_name']
	return roomDict

def printOutput(*msg):
    global output
    if output:
        print("".join(map(str,msg)))

if __name__ == "__main__":
	global agentCount, output, targetRoom, pathStrategy
	parser = argparse.ArgumentParser()
	parser.add_argument("--agents", help="the number of agents in the simulation", type=int, default=8, required=False)
	parser.add_argument("--initialRoom", help="initial room for the agents", choices=["one", "random"], default="one")
	parser.add_argument("--targetRoom", help="strategy to choose the next room for an agent", choices=["A", "B", "C"], default="A")
	parser.add_argument("--pathStrategy", help="pathing strategy", choices=["stubborn", "aborting"], default="stubborn")
	parser.add_argument("--output", help="If the program should write to the commandline", type=bool, default=False)
	parser.add_argument("--skip_animation", help="Sets parameter for comm.render_script(). If false, the simulation is prettier, but needs more frames and the agents have less time for actions towards their goals.", default=True)
	args = vars(parser.parse_args())

	agentCount = args["agents"]
	output = args["output"]
	targetRoom = args["targetRoom"]
	pathStrategy = args["pathStrategy"]

	activeAgents = {}
	neighbooringRooms= {}
	basicScript=[]
	iteration = 0
	comm.reset(0)
	success, count = comm.camera_count()
	roomDict = initRoomDict()
	initiativeBelief = np.random.rand(agentCount,agentCount) #[observer][observed]
	roomBelief = np.random.rand(agentCount,len(roomDict.keys())) #[observer][observed]


	spawnAgents(agentCount, args["initialRoom"]=="one")

	while(True):
		printOutput("Start script generation")
		skript = createMoveAgentsScript(calculateStep())
		printOutput("\n\nSkript:",skript)		
		success, message = comm.render_script(script=skript,
										processing_time_limit=6000,
										find_solution=False,
										camera_mode=[str(count-1)],
										recording=True, frame_rate=1, skip_animation=args["skip_animation"])
		printOutput("Success: ", success)
		if not success:
			printOutput("Failure while rendering: ")
			printOutput(message)
			exit(0)
		printOutput("----------------------", iteration)
		if(skript == []):
			printOutput("Finished by", iteration)
			exit(0)
		iteration += 1
