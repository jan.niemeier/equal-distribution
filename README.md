# Equal Distribution of characters in virtual home

### Basic Idea

In itself, the goal of our study can be broken down into a very simple goal: Distribute a number of people across a VirtualHome apartment without any explicit communication between the agents. To make the study more realistic, the individuals weren't granted any global knowledge apart from the number of total agents as well as the number of rooms in the apartment. The only form of communication we allowed was observation by sight. This very primitive communication method served as a simple but effective means to sense the number of agents in a room.  In our view, this makes for an interesting and more realistic middle ground between an algorithmic solution which would require more knowledge as well as a SLAM-like solution which would require less knowledge about the surroundings but would need a lot more processing power. This comes with a few caveats which will be discussed in greater detail in the following sections.

### How to use our program

To use our program, you need to install the VirtualHome environment. Follow the instructions at https://github.com/xavierpuigf/virtualhome\#set-up. If 

`pip install -r requirements.txt`

fails, it might be necessary necessary to use a newer version of some packages. Replace the respective lines in the requirements file with


`certifi==2020.06.20`

`opencv-python==4.1.2.30`

`numpy==1.17.3`


Place the script.py file from our Github repository (https://gitlab.ub.uni-bielefeld.de/jan.niemeier/equal-distribution/) in the virtualhome folder. Open the unity executable and start the script.py with
python3 script.py

## Logic of the Program

### Knowledge

Every agent has knowledge about the total number of agents, the number of rooms in the house and the goal they have to achieve. They also held some belief about each of the other agents and in some cases about the rooms. We also decided that agents were able to see all the other agents that were in a room with them and could recognize them later. However, they can not look into other rooms, unless they entered them. 

### Initiative

In the initial state the agents are placed in one room. We therefore had to find a way to make every agent that was within an overpopulated room realize that someone had to leave this room and decide which one of them it should be.  Here the agents had to interact with each other. We could not let them make this decision independently, because that could lead to all of them leaving the room at the same time and leading the room to now be underpopulated.  We needed to construct a decision process that would work without direct communication between the agents and ensured that as few agents as possible would leave a room at the same time. Once we managed that, we could expect that agents would keep leaving the room until it held the exact number of agents it required. We implemented this through a notion of initiative. Each agent is therefore equipped with an internal belief on how proactive they are compared with their peers. These initiative values are assigned randomly and range from 0 to 1. A higher value corresponds to being more proactive. Each agent only really knows their own initiative. 

For more information please see our report. 
